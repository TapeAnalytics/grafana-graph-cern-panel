# Long Term Grouping Graph Panel

Allows to perform grouping per natural month and year. 

Configure it in the Axis tab, X-axis when mode is Time.

Webpack copy of [Grafana default panel](http://docs.grafana.org/features/panels/graph/). 

Works only on Grafana versions >= V5.0.1 

# Build

```
npm install
npm run build
```

# Credits

Based on 

* [grafana-graph-panel](https://github.com/CorpGlory/grafana-graph-panel)
* [grafana-plugin-template-webpack-typescript](https://github.com/CorpGlory/grafana-plugin-template-webpack-typescript) 
* [@types/grafana](https://github.com/CorpGlory/types-grafana)
